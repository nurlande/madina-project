import React from 'react'
import CIcon from '@coreui/icons-react'
import {
  cilSpeedometer,
} from '@coreui/icons'
import { CNavItem } from '@coreui/react'

const _nav = [
  // {
  //   component: CNavItem,
  //   name: 'Home',
  //   to: '/home',
  //   icon: <CIcon icon={cilSpeedometer} customClassName="nav-icon" />
  // },
  {
    component: CNavItem,
    name: 'Товары',
    to: '/items',
    icon: <CIcon icon={cilSpeedometer} customClassName="nav-icon" />
  },
  {
    component: CNavItem,
    name: 'История товаров',
    to: '/item-history',
    icon: <CIcon icon={cilSpeedometer} customClassName="nav-icon" />
  },
  {
    component: CNavItem,
    name: 'Проданы',
    to: '/sold',
    icon: <CIcon icon={cilSpeedometer} customClassName="nav-icon" />
  },
]

export default _nav
