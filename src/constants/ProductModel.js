export const ProductModel = () =>  {
    return {
        name: "",
        price: 0,
        link: "",
        mark: "",
        category: "",
        subCategory: "",
        image: "",
        priceOriginal: "",
        description: "",
        color: "",
        size: [],
        createdAt: new Date()
    }
}