import { SETROLLS } from '../types/rollsTypes'

export const setRolls = rolls => ({ type: SETROLLS, payload: rolls })

