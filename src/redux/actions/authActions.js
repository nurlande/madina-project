import {
  USER_SUCCESS,
  USER_LOADING,
  USER_FAIL,
  LOGOUT,
  LOGIN_FAIL,
  LOGIN_SUCCESS,
  LOGIN_LOADING,
  CLEAR_ERROR,
} from "../types/authTypes"


export const loadUser = () => async (dispatch, getState) => {

}

export const login = (username, password) => async (dispatch, getState) => {

}

export const loginLoading = () => ({ type: LOGIN_LOADING })
export const loginSuccess = (user, accessToken, refreshToken) => ({ type: LOGIN_SUCCESS, payload: {user, accessToken, refreshToken} })
export const loginFail = error => ({ type: LOGIN_FAIL, payload: error })

export const userLoading = () => ({ type: USER_LOADING })
export const userSuccess = (user, accessToken) => ({ type: USER_SUCCESS, payload: {user, accessToken} })
export const userFail = error => ({ type: USER_FAIL, payload: error })

export const logout = () => ({ type: LOGOUT })

export const clearError = () => ({ type: CLEAR_ERROR })
