import { SET, MODE, BRANCH, UNFOLD} from '../types/settingsTypes'

export const setSidebarShow = sidebarShow => ({ type: SET, payload: sidebarShow })

export const setSidebarUnfold = sidebarUnfoldable => ({ type: UNFOLD, payload: sidebarUnfoldable })

export const setMode = mode => ({ type: MODE, payload: mode })

export const setBranch = branch => ({ type: BRANCH, payload: branch })
