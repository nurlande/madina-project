import { ADDTOCART, EMPTYCART, REMOVEFROMCART, UPDATECART } from '../types/productTypes'

export const addToCart = product => ({ type: ADDTOCART, payload: product })

export const removeFromCart = product => ({ type: REMOVEFROMCART, payload: product })

export const emptyCart = () => ({ type: EMPTYCART })

export const updateAmountCart = (id, amount) => ({ type: UPDATECART, id: id, amount: amount })

