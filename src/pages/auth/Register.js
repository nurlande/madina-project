import React, {useState} from 'react'
import {
  CAlert,
  CButton,
  CCard,
  CCardBody,
  CCol,
  CContainer,
  CForm,
  CFormInput,
  CFormSelect,
  CInputGroup,
  CInputGroupText,
  CRow,
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { cilLockLocked, cilUser } from '@coreui/icons'
import {createUserWithEmailAndPassword, onAuthStateChanged} from "@firebase/auth"
import { fire, fireAuth } from 'src/configs/fire'
import { useHistory } from 'react-router'
import { doc, setDoc } from 'firebase/firestore'
import { useDispatch } from 'react-redux'
import { setMode, setBranch as setBranchRedux } from 'src/redux/actions/settingsActions'

const Register = () => {

    const [branch, setBranch] = useState("bishkek");
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("")

    const [response, setResponse] = useState(null)
    const [loading, setLoading] = useState(null)

    onAuthStateChanged(fireAuth, currentUser => {
        localStorage.setItem("madinaUser", JSON.stringify(currentUser))
    })

    const history = useHistory()
    const dispatch = useDispatch()

    const submitRegister = e => {
        e.preventDefault()
        setLoading(true)
        console.log("e", email)
        console.log("p", password)
        const user = createUserWithEmailAndPassword(fireAuth, email, password)
        user.then(u => {
          console.log(u)
          let payload = {uid: u.user.uid, email: u.user.email, role: "viewer", name, password, branch}
          setDoc(doc(fire, "users", u.user.uid), payload).then(res => {
            setResponse("success")
            setLoading(false)
            dispatch(setMode("viewer"))
            dispatch(setBranchRedux(branch === "almaty" ? "almaty" : ""))
            history.push("/items")
          })
        }).catch(err => {
            setResponse("failed")
            setLoading(false)
            console.log(err)
        })
    }

  return (
    <div className="bg-light min-vh-100 d-flex flex-row align-items-center">
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md={9} lg={7} xl={6}>
            <CCard className="mx-4">
              <CCardBody className="p-4">

                {response && 
                  <CAlert color={response === "success" ? "success" : "danger"} className="text-capitalize">
                    {response}
                  </CAlert>}
                <CForm onSubmit={submitRegister}>
                  <h1>Регистрация</h1>
                  <p className="text-medium-emphasis">Создать новый аккаунт</p>
                  <CInputGroup className='mb-3'>
                    <CFormSelect onChange={(e) => setBranch(e.target.value)} value={branch}>
                      <option value="bishkek">Бишкек</option>
                      <option value="almaty">Алматы</option>
                    </CFormSelect>
                  </CInputGroup>
                  <CInputGroup className="mb-3">
                    <CInputGroupText>
                      <CIcon icon={cilUser} />
                    </CInputGroupText>
                    <CFormInput placeholder="Name" onChange={e => setName(e.target.value)} />
                  </CInputGroup>
                  <CInputGroup className="mb-3">
                    <CInputGroupText>@</CInputGroupText>
                    <CFormInput placeholder="Email" autoComplete="email" onChange={e => setEmail(e.target.value)}/>
                  </CInputGroup>
                  <CInputGroup className="mb-3">
                    <CInputGroupText>
                      <CIcon icon={cilLockLocked} />
                    </CInputGroupText>
                    <CFormInput
                      type="password"
                      placeholder="Password"
                      autoComplete="new-password"
                      onChange={e => setPassword(e.target.value)}
                    />
                  </CInputGroup>
                  <div className="d-grid">
                    <CButton color="success" disabled={loading} type="submit">Зарегистрировать</CButton>
                  </div>
                </CForm>
              </CCardBody>
            </CCard>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  )
}

export default Register
