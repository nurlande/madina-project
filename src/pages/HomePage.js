import React from 'react'
import Items from './Items'

export default function HomePage() {

    return (
        <div>
            <Items />
        </div>
    )
}