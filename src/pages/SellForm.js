import { CButton, CCard, CCardBody, CCol, CForm, CFormInput, CFormLabel, CRow, CTable, CTableBody, CTableFoot, CTableHead } from '@coreui/react'
import { addDoc, collection, doc, onSnapshot, query, setDoc, writeBatch } from 'firebase/firestore';
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import Creatable from 'react-select/creatable';
import Select from 'react-select';
import { fire } from 'src/configs/fire';
import CIcon from '@coreui/icons-react';
import { cilMinus, cilPlus } from '@coreui/icons';
import MiniSpinner from 'src/components/spinners/MiniSpinner';
import { toastify } from 'src/helpers/toast';
import { setRolls } from 'src/redux/actions/rollsActions';


export default function SellForm () {

    const dispatch = useDispatch()

    const rolls = useSelector(state => state.rolls.rolls)
    const branch = useSelector(state => state.settings.branch)

    const [clients, setClients] = useState([])
    const [items, setItems] = useState([])

    const [selectedClient, setSelectedClient] = useState(null)
    const [selectedItem, setSelectedItem] = useState(null)
    const [selectedColor, setSelectedColor] = useState(null)

    const [selected, setSelected] = useState([])

    const [price, setPrice] = useState("")

    const [loading, setLoading] = useState(false)


    useEffect(() => {
        const qc = query(collection(fire, "clients" + branch))
        onSnapshot(qc, (querySnapshot) => {
            setClients(querySnapshot.docs.map(d => {
                return {id: d.id, ...d.data()}
            }))
        })

        const qi = query(collection(fire, "items" + branch))
        onSnapshot(qi, (querySnapshot) => {
            setItems(querySnapshot.docs.map(d => {
                return {id: d.id, ...d.data()}
            }))
        })
    }, [branch])

    const updateSelected = (roll) => {
        if (selected.some(s => s.id === roll.id)) {
            setSelected(selected.filter(s => s.id !== roll.id))
        } else {
            setSelected([...selected, {...roll, item: selectedItem}])
        }
    }

    const submitSell = () => {
        setLoading(true)
        if(!selectedClient.id) {
                const collectionRef = collection(fire, "clients" + branch);
                addDoc(collectionRef, {name: selectedClient.label, phone: selectedClient.phone}).then(res => {
                    setSelectedClient({...selectedClient, id: res.id, ...res})
                    sellSelected({name: selectedClient.label, phone: selectedClient.phone, id: res.id}, price)
                }).catch(err => {
                    toastify("error", "Operation failed")
                    setLoading(false)
                    console.log(err)
                })
        } else {
                setDoc(doc(fire, "clients" + branch, selectedClient.id), 
                        {name: selectedClient.label, phone: selectedClient.phone}).then(res => {
                    sellSelected({name: selectedClient.label, phone: selectedClient.phone, id: selectedClient.id}, price)
                }).catch(err => {
                    toastify("error", "Operation failed")
                    setLoading(false)
                    console.log(err)
                })
        }
    }

    const sellSelected = (client, pricePerM) => {

        const batch = writeBatch(fire)
        const soldCollectionRef = collection(fire, "sold" + branch);
        selected.forEach(s => {
            let payload = {color: s.color, item: s.item, roll: s, soldAt: new Date(), client, pricePerM}
            console.log(payload)
            batch.set(doc(fire, "sold" + branch, doc(soldCollectionRef).id), payload)
            batch.delete(doc(fire, "rolls" + branch, s.id))
        })

        batch.commit().then(res => {
            setLoading(false)
            toastify("success", "Успешная операция")
            const q = query(collection(fire, "rolls" + branch))
            onSnapshot(q, (querySnapshot) => {
                dispatch(setRolls(
                    querySnapshot.docs.map(d => {
                        return {id: d.id, ...d.data()}
                    })
                    ))
                })
            setSelected([])
            setPrice("")
            setSelectedColor(null)
            setSelectedItem(null)
        }).catch(err => setLoading(false))
    }

    return (
        <CCard>
            <CCardBody>
                {loading ? <MiniSpinner/> : <CForm>
                    <CRow className='my-3'>
                        <CCol sm="4">
                        <CFormLabel>Клиент</CFormLabel>
                            <Creatable 
                                formatCreateLabel={res => "Создать " + res}
                                options={clients.map(c => {return {label: c.name, value: c.id, ...c}})}
                                onChange={res => {
                                    setSelectedClient(res)
                                }}
                                value={selectedClient}
                            />
                        {selectedClient ? <div>
                            <CFormLabel>Телефон</CFormLabel>
                            <CFormInput placeholder='телефон' type="text" 
                            // disabled={selectedClient?.id || false} 
                                onChange={e => setSelectedClient({...selectedClient, phone: e.target.value})} 
                                value={selectedClient?.phone || ""}/>
                        </div> : ""}
                        </CCol>
                        <CCol sm="4">
                            <CFormLabel>Товар</CFormLabel>
                            <Select
                                isClearable
                                options={items.map(it => {return {label: it.name, value: it.id, ...it}})}
                                onChange={res => setSelectedItem(res)}
                                value={selectedItem}
                            />
                        {selectedItem ? <div>
                            <CFormLabel>Цвет</CFormLabel>
                            <Select
                                isClearable
                                options={selectedItem.colors?.map(c => {return {label: c, value: c}})}
                                onChange={res => setSelectedColor(res)}
                                value={selectedColor}
                            />
                        </div> : ""}
                        </CCol>
                        <CCol sm="4">
                            <CFormLabel>Цена за метр</CFormLabel>
                            <CFormInput placeholder='price per m' type="number" onChange={e => setPrice(e.target.value)} value={price}/>
                        </CCol>
                    </CRow>
                    <CRow>
                        <CCol sm="6">
                            <h5>Рулоны</h5>
                            <hr />
                            <div className='table-responsive-sm'>
                                <CTable className='table'>
                                    <CTableBody>
                                        {rolls.filter(r => r.color === selectedColor?.value && r.itemId === selectedItem?.id)
                                            .sort((a,b) => parseFloat(a.len) > parseFloat(b.len) ? 1 : -1).map((r, i) =>
                                            <tr key={i}>
                                                <td>{i+1}. </td>
                                                <td style={{width: "92%"}}>
                                                    {r.len} м
                                                </td>
                                                <td>
                                                    <span onClick={() => updateSelected(r)} className='mx-auto' style={{cursor: "pointer"}}>
                                                        {selected.some(s => s.id === r.id) ? 
                                                        <CIcon icon={cilMinus}/> : <CIcon icon={cilPlus}/>}
                                                    </span>
                                                </td>
                                            </tr>
                                        )}
                                    </CTableBody>
                                </CTable>
                            </div>
                        </CCol>
                        <CCol sm="6">
                            <h5>Выбранные</h5>
                            <hr />
                            {selected.length > 0 && <div className='table-responsive-sm'>
                                <CTable className='table'>
                                    <CTableHead>
                                        <th>Товар</th>
                                        <th>Цвет</th>
                                        <th>Длина</th>
                                        <th></th>
                                    </CTableHead>
                                    <CTableBody>
                                        {selected.map((r, i) =>
                                            <tr key={i}>
                                                <td>
                                                    {r.item?.name}
                                                </td>
                                                <td>
                                                    {r.color}
                                                </td>
                                                <td>
                                                    {r.len} м
                                                </td>
                                                <td>
                                                    <span onClick={() => updateSelected(r)} className='mx-auto' style={{cursor: "pointer"}}>
                                                        {selected.some(s => s.id === r.id) ? 
                                                        <CIcon icon={cilMinus}/> : <CIcon icon={cilPlus}/>}
                                                    </span>
                                                </td>
                                            </tr>
                                        )}
                                    </CTableBody>
                                    <CTableFoot>
                                        <tr>
                                            <td colSpan="2">
                                                <b>Количество:</b>
                                            </td>
                                            <td>
                                                <b>{selected.length} </b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colSpan="2">
                                                <b>Общая длина:</b>
                                            </td>
                                            <td>
                                                <b>{selected.reduce((sum, s) => sum + parseFloat(s.len), 0)} м </b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colSpan="2">
                                                <b>Общая цена:</b>
                                            </td>
                                            <td>
                                                <b>
                                                    {selected.reduce((sum, s) => sum + (parseFloat(s.len) * parseFloat(price) || 0), 0)} 
                                                {branch === "almaty" ? " KZT" : " KGS"}
                                                </b>
                                            </td>
                                        </tr>
                                    </CTableFoot>
                                </CTable>
                            </div>}
                            <div className='d-flex flex-row-reverse'>
                                <CButton color='success' onClick={submitSell} disabled={loading}>
                                    Продать
                                </CButton>
                            </div>
                        </CCol>
                    </CRow>
                </CForm>}
            </CCardBody>
        </CCard>
    )
}