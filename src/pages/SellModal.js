import { CButton, CFormInput, CFormLabel, CModal, CModalBody, CModalFooter, CModalHeader} from '@coreui/react'
import React, { useEffect, useState } from 'react'
import { addDoc, collection, doc, onSnapshot, query, writeBatch } from "firebase/firestore";
import { fire} from "src/configs/fire";
import { useDispatch, useSelector } from 'react-redux';
import { toastify } from 'src/helpers/toast';
import { setRolls } from 'src/redux/actions/rollsActions';
import Creatable from 'react-select/creatable';

export default function SellModal({selected, setSelected, open, setOpen, item, color, loading, setLoading}) {

    const dispatch = useDispatch()

    const branch = useSelector(state => state.settings.branch)

    const [clients, setClients] = useState([])
 
    const [selectedClient, setSelectedClient] = useState(null)
    const [price, setPrice] = useState("")
 

    useEffect(() => {
        const q = query(collection(fire, "clients" + branch))
        onSnapshot(q, (querySnapshot) => {
            setClients(querySnapshot.docs.map(d => {
                return {id: d.id, ...d.data()}
            }))
        })
    }, [branch])

    const sellSelected = (client, pricePerM) => {

        const batch = writeBatch(fire)
        const soldCollectionRef = collection(fire, "sold" + branch);
        selected.forEach(s => {
            let payload = {color, item, roll: s, soldAt: new Date(), client, pricePerM}
            console.log(payload)
            batch.set(doc(fire, "sold" + branch, doc(soldCollectionRef).id), payload)
            batch.delete(doc(fire, "rolls" + branch, s.id))
        })

        batch.commit().then(res => {
            setLoading(false)
            toastify("success", "Успешная операция")
            setSelected([])
            const q = query(collection(fire, "rolls" + branch))
            onSnapshot(q, (querySnapshot) => {
                dispatch(setRolls(
                    querySnapshot.docs.map(d => {
                        return {id: d.id, ...d.data()}
                    })
                ))
            })
            setOpen(false)
            setSelectedClient(null)
            setPrice("")
        }).catch(err => setLoading(false))
    }

    const confirmSell = () => {
        setLoading(true)
        // create user or get id
        if(!selectedClient.id) {
            // if(selectedClient.label && selectedClient.phone) {
                const collectionRef = collection(fire, "clients" + branch);
                addDoc(collectionRef, {name: selectedClient.label, phone: selectedClient.phone}).then(res => {
                    console.log({...selectedClient, id: res.id}, price)
                    sellSelected({name: selectedClient.label, phone: selectedClient.phone, id: res.id}, price)
                }).catch(err => {
                    toastify("error", "Operation failed")
                    setLoading(false)
                    console.log(err)
                })
            // } else {
                
            // }
        } else {
            console.log(selectedClient, price)
            sellSelected({name: selectedClient.name, phone: selectedClient.phone, id: selectedClient.id}, price)
        }
    }

    return (
        <CModal visible={open} onClose={() => setOpen(false)}>
            <CModalHeader>
                Общая длина: {selected.reduce((sum, s) => sum + parseFloat(s.len), 0)} м
            </CModalHeader>
            <CModalBody>
                {!loading ? <>
                <CFormLabel>Клиент</CFormLabel>
                <Creatable 
                    options={clients.map(c => {return {label: c.name, value: c.id, ...c}})}
                    onChange={res => {
                        setSelectedClient(res)
                    }}
                    value={selectedClient}
                />
                {selectedClient ? <div>
                    <CFormLabel>Телефон</CFormLabel>
                    <CFormInput placeholder='телефон' type="text" disabled={selectedClient?.id || false} 
                        onChange={e => setSelectedClient({...selectedClient, phone: e.target.value})} 
                        value={selectedClient?.phone || ""}/>
                </div> : ""}
                
                <CFormLabel>Цена за метр</CFormLabel>
                <CFormInput placeholder='price per m' type="number" onChange={e => setPrice(e.target.value)} value={price}/>

                <div className='my-2'>
                    <p>Итоговая цена: {(selected.reduce((sum, s) => sum + parseFloat(s.len), 0) * price).toFixed(2)}</p>
                </div>
                </> : ""}
            </CModalBody>
            <CModalFooter>
                {!loading ? <div className="my-2 d-flex flex-row-reverse">
                    <CButton className='mx-1' color="success" disabled={!price || price==="" || loading} onClick={() => !loading && price && confirmSell()}>Продать</CButton>
                    <CButton className='mx-1' color="secondary" onClick={() => setOpen(false)}>Закрыть</CButton>
                </div> : ""}
            </CModalFooter>
        </CModal>
    )
}