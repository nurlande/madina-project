import {
  CButton,
  CCard,
  CCardBody,
  CCardHeader,
  CForm,
  CFormInput,
  CFormLabel,
} from "@coreui/react";
import React, { useEffect, useState } from "react";
import {
  addDoc,
  collection,
  doc,
  getDoc,
  onSnapshot,
  query,
  writeBatch,
} from "firebase/firestore";
import { fire } from "src/configs/fire";
import { useParams } from "react-router";
import { useTranslation } from "react-i18next";
import { useHistory } from "react-router-dom";
import { toastify } from "src/helpers/toast";
import CIcon from "@coreui/icons-react";
import { cilX } from "@coreui/icons";
import { useDispatch, useSelector } from "react-redux";
import { setRolls } from "src/redux/actions/rollsActions";

export default function RollStore() {
  const { t } = useTranslation();

  const params = useParams();
  const history = useHistory();

  const dispatch = useDispatch();

  const branch = useSelector((state) => state.settings.branch);

  const [item, setItem] = useState({
    name: "",
    colors: [""],
    image: "",
  });

  const [color, setColor] = useState(null);
  const [fieldNumber, setFieldNumber] = useState(null);

  const [lens, setLens] = useState([""]);

  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (params.id) {
      setLoading(true);
      const dRollRef = doc(fire, "items" + branch, params.id);
      getDoc(dRollRef).then((res) => {
        setItem({ ...res.data(), id: params.id });
        setLoading(false);
      });
    }
    if (params.color) {
      setColor(params.color);
    }
  }, [params, branch]);

  const saveStore = () => {
    setLoading(true);
    const batch = writeBatch(fire);
    const collectionRef = collection(fire, "rolls" + branch);

    lens
      .filter((l) => l)
      .forEach((l) => {
        let payload = { color: color, itemId: item.id, len: l };
        console.log(payload);
        batch.set(doc(fire, "rolls" + branch, doc(collectionRef).id), payload);
      });

    batch
      .commit()
      .then((res) => {
        addDoc(collection(fire, "historia" + branch), {
          date: new Date(),
          item: item,
          color: color,
          action: "прием",
          amount: lens.length,
          lensM: lens,
        }).then((res) => {
          toastify("success", "Успешная операция");
          setLoading(false);
          setLens([""]);
          const q = query(collection(fire, "rolls" + branch));
          onSnapshot(q, (querySnapshot) => {
            dispatch(
              setRolls(
                querySnapshot.docs.map((d) => {
                  return { id: d.id, ...d.data() };
                })
              )
            );
          });
        });
      })
      .catch((err) => setLoading(false));
  };

  const handleChange = (val, i) => {
    let items = [...lens];
    items[i] = val;
    setLens([...items]);
  };

  const removeInput = (i) => {
    let items = [...lens];
    console.log(i);
    items.splice(i, 1);
    setLens([...items]);
  };

  const createFields = () => {
    let newFields = [];
    for (let i = 0; i < fieldNumber; i++) {
      newFields.push("");
    }
    setLens([...lens, ...newFields]);
  };

  const goBack = () => {
    history.push("/items/" + item?.id + "/store");
  };

  return (
    <CCard className="my-2 w-75 mx-auto">
      <CCardHeader>
        {t("Рулоны")} прием: {color}
      </CCardHeader>
      <CCardBody>
        {loading ? (
          <div>Загрузка...</div>
        ) : (
          <div>
            <div className="d-flex">
              <CFormInput
                className="my-1 mx-2"
                placeholder="количество полей"
                type="number"
                value={fieldNumber}
                size="sm"
                onChange={(e) => setFieldNumber(e.target.value)}
              />
              <CButton
                color="success"
                variant="outline"
                size="sm"
                className="my-2"
                style={{ width: "20%" }}
                onClick={() => createFields()}
              >
                {t("Добавить поля")}
              </CButton>
            </div>

            <CForm>
              <CFormLabel>Длина</CFormLabel>

              {lens.map((l, i) => (
                <div className="d-flex" key={i}>
                  <CFormLabel className="my-auto mx-2">{i + 1}</CFormLabel>
                  <CFormInput
                    className="my-1"
                    placeholder="Длина"
                    type="number"
                    value={l}
                    onChange={(e) => handleChange(e.target.value, i)}
                  />
                  {lens.length !== 1 && (
                    <CIcon
                      icon={cilX}
                      className="my-auto mx-1"
                      style={{ cursor: "pointer" }}
                      onClick={() => removeInput(i)}
                    />
                  )}
                </div>
              ))}
              <CButton
                color="light"
                size="sm"
                className="my-2"
                onClick={() => setLens([...lens, ""])}
              >
                {t("Еще")}
              </CButton>

              <div className="my-2 d-flex flex-row-reverse">
                <CButton
                  className="mx-1"
                  color="success"
                  disabled={loading}
                  onClick={() => saveStore()}
                >
                  Сохранить
                </CButton>
                <CButton
                  className="mx-1"
                  color="secondary"
                  onClick={() => goBack()}
                >
                  Назад
                </CButton>
              </div>
            </CForm>
          </div>
        )}
      </CCardBody>
    </CCard>
  );
}
