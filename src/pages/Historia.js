import {
  CButton,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCardTitle,
  CCol,
  CFormInput,
  CFormLabel,
  CModal,
  CModalBody,
  CRow,
  CTable,
  CTableBody,
  CTableHead,
} from "@coreui/react";
import React, { useEffect, useState } from "react";
import { fire } from "src/configs/fire";
import {
  onSnapshot,
  collection,
  query,
  orderBy,
  doc,
  deleteDoc,
  getDocs,
} from "firebase/firestore";
import { useTranslation } from "react-i18next";
import MiniSpinner from "src/components/spinners/MiniSpinner";
import { useSelector } from "react-redux";
import { toastify } from "src/helpers/toast";
import Select from "react-select";
import { dateFormatter, dateAbsolute } from "src/helpers/dateToString";

export default function Historia() {
  const { t } = useTranslation();
  const [historia, setHistoria] = useState([]);
  const [items, setItems] = useState([]);

  const [dateFrom, setDateFrom] = useState(new Date().toJSON().slice(0, 10));
  const [dateTo, setDateTo] = useState(new Date().toJSON().slice(0, 10));
  const [itemFilter, setItemFilter] = useState("");
  const [colorFilter, setColorFilter] = useState("");
  const [actionFilter, setActionFilter] = useState("");

  const [loading, setLoading] = useState(false);
  const [selectedHistoria, setSelectedHistoria] = useState(null);
  const [modalShow, setModalShow] = useState(false);

  const branch = useSelector((state) => state.settings.branch);
  const mode = useSelector((state) => state.settings.mode);

  useEffect(() => {
    const qi = query(collection(fire, "items" + branch));
    setLoading(true);
    getDocs(qi).then((querySnapshot) => {
      setItems(
        querySnapshot.docs.map((d) => {
          return { id: d.id, ...d.data() };
        })
      );
      setLoading(false);
    });
  }, [branch]);

  useEffect(() => {
    const q = query(
      collection(fire, "historia" + branch),
      orderBy("date", "desc")
    );

    setLoading(true);
    onSnapshot(q, (querySnapshot) => {
      setHistoria(
        querySnapshot.docs.map((d) => {
          return { id: d.id, ...d.data() };
        })
      );
      setLoading(false);
    });
  }, [branch]);

  const deleteHistoria = () => {
    setLoading(true);
    deleteDoc(doc(fire, "historia" + branch, selectedHistoria.id))
      .then((res) => {
        setModalShow(false);
        setLoading(false);
        let ss = historia;
        setHistoria(ss.filter((s) => s.id !== selectedHistoria.id));
        toastify("success", "Успешное удаление");
      })
      .catch((err) => {
        console.log(err);
        toastify("error", "Ошибка");
      });
  };

  return (
    <CCard className="my-2 mx-auto">
      <CCardHeader>
        <CRow>
          <CCol>
            <CCardTitle>{t("История")}</CCardTitle>
          </CCol>
        </CRow>
      </CCardHeader>
      <CCardBody>
        {loading ? (
          <MiniSpinner />
        ) : (
          <div>
            <CRow className="my-2">
              <CCol sm="2">
                <CFormLabel>{t("Событие")}</CFormLabel>
                <Select
                  isClearable
                  options={[
                    { label: "Создание", value: "создание" },
                    { label: "Прием", value: "прием" },
                    { label: "Удаление", value: "удаление" },
                  ]}
                  onChange={(res) => setActionFilter(res)}
                  value={actionFilter}
                />
              </CCol>
              <CCol sm="2">
                <CFormLabel>{t("С")}</CFormLabel>
                <CFormInput
                  type="date"
                  onChange={(e) => setDateFrom(e.target.value)}
                  value={dateFrom}
                />
              </CCol>
              <CCol sm="2">
                <CFormLabel>{t("По")}</CFormLabel>
                <CFormInput
                  type="date"
                  onChange={(e) => setDateTo(e.target.value)}
                  value={dateTo}
                />
              </CCol>
              <CCol sm="3">
                <CFormLabel>{t("Товар")}</CFormLabel>
                <Select
                  isClearable
                  options={items.map((it) => {
                    return { label: it.name, value: it.id, ...it };
                  })}
                  onChange={(res) => setItemFilter(res)}
                  value={itemFilter}
                />
              </CCol>
              <CCol sm="3">
                <CFormLabel>{t("Цвет")}</CFormLabel>
                <Select
                  isClearable
                  options={itemFilter?.colors?.map((it) => {
                    return { label: it, value: it };
                  })}
                  onChange={(res) => setColorFilter(res)}
                  value={colorFilter}
                />
              </CCol>
            </CRow>
            <div className="table-responsive">
              <CTable className="table">
                <CTableHead>
                  <tr>
                    <th></th>
                    <th>{t("Дата")}</th>
                    <th>{t("Товар")}</th>
                    <th>{t("События")}</th>
                    {mode === "admin" && <th>{t("Действия")}</th>}
                  </tr>
                </CTableHead>
                <CTableBody>
                  {historia
                    .filter((s) =>
                      actionFilter ? s.action === actionFilter?.value : true
                    )
                    .filter((s) =>
                      itemFilter ? s.item?.id === itemFilter?.id : true
                    )
                    .filter((s) =>
                      colorFilter ? s.color === colorFilter?.label : true
                    )
                    .filter((s) =>
                      dateFrom
                        ? Date.parse(s.date.toDate()) >=
                          Date.parse(dateAbsolute(dateFrom))
                        : true
                    )
                    .filter((s) =>
                      dateTo
                        ? Date.parse(s.date.toDate()) <=
                          Date.parse(dateAbsolute(dateTo, true))
                        : true
                    )
                    .map((s, i) => (
                      <tr key={i} style={{ verticalAlign: "bottom" }}>
                        <td>{i + 1}</td>
                        <td>{dateFormatter(s.date.toDate())}</td>
                        <td>{s.item?.name}</td>
                        <td>
                          {s.action === "прием"
                            ? `прием № ${s.color},
                              ${s.amount ? s.amount + "р, " : ""} 
                              ${s.lensM ? s.lensM?.map((l) => l + " м") : ""}`
                            : s.action}
                        </td>
                        {mode === "admin" && (
                          <td>
                            <CButton
                              color="danger"
                              size="sm"
                              className="m-1"
                              onClick={() => {
                                setSelectedHistoria(s);
                                setModalShow(true);
                              }}
                            >
                              Удалить
                            </CButton>
                          </td>
                        )}
                      </tr>
                    ))}
                </CTableBody>
              </CTable>
            </div>
          </div>
        )}
      </CCardBody>
      <CCardFooter></CCardFooter>
      {selectedHistoria && (
        <CModal visible={modalShow} onClose={() => setModalShow(false)}>
          <CModalBody>
            {loading ? (
              <MiniSpinner />
            ) : (
              <>
                <p>Подвердите удаление?</p>
                <div className="d-flex flex-row-reverse">
                  <CButton color="success" onClick={() => deleteHistoria()}>
                    Подтвердить
                  </CButton>
                </div>
              </>
            )}
          </CModalBody>
        </CModal>
      )}
    </CCard>
  );
}
