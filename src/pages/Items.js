import { CButton, CCard, CCardBody, CCardFooter, CCardHeader, CCardTitle, CCol, CFormInput, CModal, CModalBody, CModalFooter, CModalHeader, CRow, CTable, CTableBody, CTableHead } from '@coreui/react'
import React, { useEffect, useState } from "react"
import { fire } from "src/configs/fire";
import { doc, onSnapshot, collection, query, deleteDoc, addDoc } from "firebase/firestore";
import { useHistory } from 'react-router';
import { useTranslation } from 'react-i18next';
import MiniSpinner from 'src/components/spinners/MiniSpinner';
import {toastify} from 'src/helpers/toast'
import {useSelector} from 'react-redux'
import default_img from 'src/assets/images/default_img.png'

export default function Items() {

    const {t} = useTranslation()

    const [items, setItems] = useState([])

    const [searchKey, setSearchKey] = useState(null)

    const [confirmModal, setConfirmModal] = useState(false)
    const [selectedItem, setSelectedItem] = useState(null)
    const [loading, setLoading] = useState(false)

    const history = useHistory()

    const mode = useSelector(state => state.settings.mode)
    const branch = useSelector(state => state.settings.branch)

    useEffect(() => {
        const q = query(collection(fire, "items" + branch))
        setLoading(true)
        onSnapshot(q, (querySnapshot) => {
            setItems(querySnapshot.docs.map(d => {
                return {id: d.id, ...d.data()}
            }))
            setLoading(false)
        })
    }, [branch])

    const deleteModal = (item) => {
        setConfirmModal(true)
        setSelectedItem(item)
    }

    const confirmDelete = () => {
        deleteDoc(doc(fire, "items" + branch, selectedItem.id)).then(r => {
            addDoc(collection(fire, "historia" + branch), {date: new Date(), item: selectedItem, action: "удаление"}).then(res => {                
                console.log("deleted", r)
                setConfirmModal(false)
                toastify("success", "Успешное удаление")
            })
        }).catch(err => {
            console.log(err)
            toastify("error", "Ошибка")
        })
    }

    const searchFor = (val) => {
        setSearchKey(val)
    }

    return (
        <CCard className="my-2 mx-auto">
            <CCardHeader>
                <CRow>
                    <CCol>
                        <CCardTitle>
                            {t("Товары")}
                        </CCardTitle>
                    </CCol>
                    <CCol className="d-flex flex-row-reverse">
                        {mode === "admin" && 
                            <>
                                <CButton className="float-right mx-1" color="success" onClick={() => history.push("/items/create")}>
                                    {t("Создать")}
                                </CButton>
                                <CButton className="float-right mx-1" color="primary" onClick={() => history.push("/sell-form")}>
                                    {t("Продажа")}
                                </CButton>
                            </>}
                    </CCol>
                </CRow>
            </CCardHeader>
            <CCardBody>
                {loading ? <MiniSpinner/> : 
                    <div>
                        <CFormInput placeholder='Поиск...' onChange={e => searchFor(e.target.value)}/>
                        <div className="table-responsive">
                        <CTable className="table">
                            <CTableHead>
                                <tr>
                                    <th style={{width: "5%"}}></th>
                                    <th>{t("Картина")}</th>
                                    <th>{t("Название")}</th>
                                    {mode === "admin" && <th>{t("Действия")}</th>}
                                </tr>
                            </CTableHead>
                            <CTableBody>
                                {(searchKey ? items.filter(it => 
                                    it.id?.toLowerCase().includes(searchKey.toLowerCase()) || 
                                    it.name?.toLowerCase().includes(searchKey.toLowerCase())) : items)
                                .map((it, i) => 
                                    <tr key={i} style={{verticalAlign: "bottom"}}>
                                        <td>{i + 1}. </td>
                                        <td onClick={() => history.push("/items/"+it.id)} style={{cursor: "pointer"}}>
                                            <img className='border rounded p-1 mx-1' src={it.image || default_img} alt="" height="70"/>
                                        </td>
                                        <td onClick={() => history.push("/items/"+it.id)} style={{cursor: "pointer"}}>{it.name}</td>
                                        {mode === "admin" && <td style={{width: "30%"}}>
                                            <CButton onClick={() => history.push("/items/edit/" + it.id)} className="m-1 btn-sm"
                                                variant="outline" color="warning">{t("Редактировать")}</CButton>
                                            <CButton onClick={() => deleteModal(it)} className="m-1 btn-sm"
                                                color="danger" variant="outline">{t("Удалить")}</CButton>
                                        </td>}
                                    </tr>
                                    )}
                            </CTableBody>
                        </CTable>
                        <CModal visible={confirmModal} onClose={() => setConfirmModal(false)}>
                            <CModalHeader>{t("Подтверждение")}</CModalHeader>
                            <CModalBody>
                                {t("Вы уверены удалить")} {selectedItem?.name} ?
                            </CModalBody>
                            <CModalFooter>
                                <CButton onClick={confirmDelete}>{t("Подтвердить")}</CButton>
                            </CModalFooter>
                        </CModal>
                        </div>
                    </div>}
            </CCardBody>
            <CCardFooter>

            </CCardFooter>
        </CCard>
    )
}