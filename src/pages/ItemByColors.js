import { CButton, CCard, CCardBody, CCardHeader, CListGroup, CListGroupItem, CTable, CTableBody} from '@coreui/react'
import React, { useEffect, useState } from 'react'
import { doc, getDoc } from "firebase/firestore";
import { fire} from "src/configs/fire";
import { useHistory, useParams } from 'react-router';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';

export default function ItemByColors() {

    const {t} = useTranslation()

    const params = useParams()
    const history = useHistory()

    const rolls = useSelector(state => state.rolls.rolls)
    const branch = useSelector(state => state.settings.branch)

    const [item, setItem] = useState({
        name: "",
        colors: [],
        image: ""
    })

    const [loading, setLoading] = useState(false)
    

    useEffect(() => {
        if(params.id) {
            setLoading(true)
            const dRef = doc(fire, "items" + branch, params.id)
            getDoc(dRef).then(res => {
                setItem({...res.data(), id: params.id})
                setLoading(false)
            })
        }
    }, [params, branch])

    return (
        <CCard className="my-2 w-75 mx-auto">
            <CCardHeader>
                {t("Товар")} - цвета
            </CCardHeader>
            <CCardBody>
                {loading ? 
                    <div>Загрузка...</div> : 
                    <div>
                        <div className='table-responsive-sm'>
                            <CTable className='table'>
                                <CTableBody>
                                    {item.colors?.sort((a,b) => parseInt(a) > parseInt(b) ? 1 : -1)?.map((c, i) =>
                                        <tr key={i} style={{cursor: "pointer"}} onClick={() => history.push("/items/" + item?.id + "/" + params?.action + "/" + c)}>
                                            <td>
                                                {c}
                                            </td>
                                            <td>
                                                {rolls?.filter(r => r.color === c && r.itemId === item.id).length || 0} шт
                                            </td>
                                            <td>
                                                {(rolls?.filter(r => r.color === c && r.itemId === item.id).reduce((sum, r) => sum + parseFloat(r.len), 0) || 0).toFixed(2)} м
                                            </td>
                                        </tr>
                                    )}
                                </CTableBody>
                            </CTable>
                        </div>
                        <CListGroup className='d-none'>
                            {item.colors?.map((c, i) =>
                                <CListGroupItem key={i} style={{cursor: "pointer"}} onClick={() => history.push("/items/" + item?.id + "/" + params?.action + "/" + c)}>
                                    <span className='mx-3'>{c}</span> 
                                    <span className='mx-3'>
                                        {rolls?.filter(r => r.color === c && r.itemId === item.id).length || 0} шт
                                    </span>
                                    <span className='mx-3'>
                                        {rolls?.filter(r => r.color === c && r.itemId === item.id).reduce((sum, r) => sum + parseFloat(r.len), 0) || 0} м
                                    </span>
                                </CListGroupItem>
                            )}
                        </CListGroup>
                        <div className="my-2 d-flex flex-row-reverse">
                            <CButton className='mx-1' color="secondary" onClick={() => history.push("/items/" + item?.id)}>Назад</CButton>
                        </div>
                    </div>}
            </CCardBody>
        </CCard>
    )
}