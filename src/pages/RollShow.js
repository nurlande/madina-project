import { CButton, CCard, CCardBody, CCardHeader, CTable, CTableBody} from '@coreui/react'
import React, { useEffect, useState } from 'react'
import { doc, getDoc } from "firebase/firestore";
import { fire} from "src/configs/fire";
import { useParams } from 'react-router';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';
import { useSelector } from 'react-redux';

export default function RollShow() {

    const {t} = useTranslation()

    const params = useParams()
    const history = useHistory()

    const rolls = useSelector(state => state.rolls.rolls)
    const branch = useSelector(state => state.settings.branch)

    const [item, setItem] = useState({
        name: "",
        colors: [],
        image: ""
    })

    const [color, setColor] = useState(null)

    const [loading, setLoading] = useState(false)

    useEffect(() => {
        if(params.id) {
            setLoading(true)
            const dRollRef = doc(fire, "items" + branch, params.id)
            getDoc(dRollRef).then(res => {
                setItem({...res.data(), id: params.id})
                setLoading(false)
            })
        }
        if(params.color) {
            setColor(params.color)
        }
    }, [params, branch])

    const goBack = () => {
        history.push("/items/" + item?.id)
    }

    return (
        <CCard className="my-2 w-75 mx-auto">
            <CCardHeader>
                {t("Рулоны")}: {color}
            </CCardHeader>
            <CCardBody>
                {loading ? 
                    <div>Загрузка...</div> : 
                    <div>
                        <div className='table-responsive-sm'>
                            <CTable className='table'>
                                <CTableBody>
                                    {rolls.filter(r => r.color === color && r.itemId === item?.id).map((r, i) =>
                                        <tr key={i}>
                                            <td style={{width: "100%"}}>
                                                {r.len} м
                                            </td>
                                        </tr>
                                    )}
                                </CTableBody>
                            </CTable>
                        </div>
                        <div className="my-2 d-flex flex-row-reverse">
                            <CButton className='mx-1' color="secondary" onClick={goBack}>Назад</CButton>
                        </div>
                    </div>}
            </CCardBody>
        </CCard>
    )
}