import { CButton, CCard, CCardBody, CCardHeader, CCardTitle, CCol, CRow, CTable, CTableBody} from '@coreui/react'
import React, { useEffect, useState } from 'react'
import { doc, getDoc} from "firebase/firestore";
import { fire} from "src/configs/fire";
import { useHistory, useParams } from 'react-router';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import default_img from 'src/assets/images/default_img.png'


export default function ItemDetails() {

    const {t} = useTranslation()

    const params = useParams()
    const history = useHistory()

    const rolls = useSelector(state => state.rolls.rolls)
    const mode = useSelector(state => state.settings.mode)
    const branch = useSelector(state => state.settings.branch)

    const [item, setItem] = useState({
        name: "",
        colors: [],
        image: ""
    })

    const [loading, setLoading] = useState(false)
    const [showMore, setShowMore] = useState(false)
    

    useEffect(() => {
        if(params.id) {
            setLoading(true)
            const dRef = doc(fire, "items" + branch, params.id)
            getDoc(dRef).then(res => {
                setItem({...res.data(), id: res.id})
                setLoading(false)
            })
        }
    }, [params, branch])
    
    return (
        <CCard className="my-2 w-75 mx-auto">
            <CCardHeader>
                <CRow>
                    <CCol>
                        <CCardTitle>
                       {t("Товар")} - Детали
                        </CCardTitle>
                    </CCol>
                    <CCol className="d-flex flex-row-reverse">
                        {mode === "admin" && <CButton className="float-right" color="warning" onClick={() => history.push("/items/edit/" + item?.id)}>
                            {t("Редактировать")}
                        </CButton>}
                    </CCol>
                </CRow>
            </CCardHeader>
            <CCardBody>
                {loading ? 
                    <div>Загрузка...</div> : 
                    <div>
                        <CRow>
                            <CCol sm="4">
                                <img alt="" src={item?.image || default_img} style={{maxWidth: "100%"}} className='border rounded p-1'/>
                            </CCol>
                            <CCol sm="8">
                                Товар: {item?.name} <br/>
                                Количество: {rolls.filter(r => r.itemId === item.id).length || 0} <br/>
                                Общая длина: {(rolls.filter(r => r.itemId === item.id).reduce((sum, r) => sum + parseFloat(r.len), 0) || 0).toFixed(2)} м
                                <div className='my-2'>
                                {mode === "admin" && <>
                                    <CButton className="m-1" color="primary" onClick={() => history.push("/items/" + params?.id + "/store/")}>Прием</CButton>
                                    <CButton className="m-1" color="success" onClick={() => history.push("/items/" + params?.id + "/sell/")}>Продажа</CButton>
                                </>}
                                    <CButton className="m-1" variant='outline' color="primary" onClick={() => setShowMore(!showMore)}>
                                        {showMore ? "Cкрывать по цветам" : "Показать по цветам"}
                                    </CButton>
                                </div>
                                {showMore ? 
                                    <div className='table-responsive-sm'>
                                        <CTable className='table'>
                                            <CTableBody>
                                                {item.colors?.sort((a,b) => parseInt(a) > parseInt(b) ? 1 : -1)?.map((c, i) =>
                                                    <tr key={i} style={{cursor: "pointer"}} onClick={() => history.push("/items/" + item?.id + "/show/" + c)}>
                                                        <td>
                                                            {c} 
                                                        </td>
                                                        <td>
                                                            {rolls?.filter(r => r.color === c && r.itemId === item.id).length || 0} шт
                                                        </td>
                                                        <td>
                                                            {(rolls?.filter(r => r.color === c && r.itemId === item.id).reduce((sum, r) => sum + parseFloat(r.len), 0) || 0).toFixed(2)} м
                                                        </td>
                                                    </tr>
                                                )}
                                            </CTableBody>
                                        </CTable>
                                    </div> : ""}
                            </CCol>
                        </CRow>
                    </div>}
            </CCardBody>
        </CCard>
    )
}