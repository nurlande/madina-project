import { CButton, CCard, CCardBody, CCardFooter, CCardHeader, CCardTitle, CCol, CFormInput, CFormLabel, CModal, CModalBody, CRow, CTable, CTableBody, CTableHead } from '@coreui/react'
import React, { useEffect, useState } from "react"
import { fire } from "src/configs/fire";
import { onSnapshot, collection, query, orderBy, deleteDoc, doc, setDoc, writeBatch, addDoc  } from "firebase/firestore";
import { useTranslation } from 'react-i18next';
import MiniSpinner from 'src/components/spinners/MiniSpinner';
import { useSelector } from 'react-redux';
import Select from 'react-select';
import { toastify } from 'src/helpers/toast';
import { dateFormatter, dateAbsolute } from 'src/helpers/dateToString';

export default function Sold() {

    const {t} = useTranslation()
    const [sold, setSold] = useState([])
    const [items, setItems] = useState([])
    const [clients, setClients] = useState([])

    const [dateFrom, setDateFrom] = useState(new Date().toJSON().slice(0, 10))
    const [dateTo, setDateTo] = useState(new Date().toJSON().slice(0, 10))
    const [itemFilter, setItemFilter] = useState("")
    const [colorFilter, setColorFilter] = useState("")
    const [clientFilter, setClientFilter] = useState("")

    const [loading, setLoading] = useState(false)
    const [selectedSold, setSelectedSold] = useState(null)
    const [modalFor, setModalFor] = useState(null)
    const [modalShow, setModalShow] = useState(false)
    const [btnDisabled, setBtnDisabled] = useState(false)
    
    const [clientModalShow, setClientModalShow] = useState(false)
    const [newClient, setNewClient] = useState(null)

    const branch = useSelector(state => state.settings.branch)
    const mode = useSelector(state => state.settings.mode)

    useEffect(() => {
        const q = query(collection(fire, "sold" + branch), orderBy("soldAt", "desc"))
        setLoading(true)
        onSnapshot(q, (querySnapshot) => {
            setSold(querySnapshot.docs.map(d => {
                return {id: d.id, ...d.data()}
            }))
            setLoading(false)
        })

        const qi = query(collection(fire, "items" + branch))
        setLoading(true)
        onSnapshot(qi, (querySnapshot) => {
            setItems(querySnapshot.docs.map(d => {
                return {id: d.id, ...d.data()}
            }))
            setLoading(false)
        })
        const qc = query(collection(fire, "clients" + branch))
        setLoading(true)
        onSnapshot(qc, (querySnapshot) => {
            setClients(querySnapshot.docs.map(d => {
                return {id: d.id, ...d.data()}
            }))
            setLoading(false)
        })
    }, [branch])


    const deleteSold = () => {
        setLoading(true)
        setBtnDisabled(true)
        console.log("delete", selectedSold.id)
        deleteDoc(doc(fire, "sold" + branch, selectedSold.id)).then(res => {
            console.log("deleted", res)
            let payload = {color: selectedSold.color, itemId: selectedSold.item.id, len: selectedSold.roll?.len}
            if(modalFor === "return") { 
                addDoc(collection(fire, "rolls" + branch), payload).then(res => {
                    setLoading(false)
                    setBtnDisabled(false)
                    let ss = sold
                    setSold(ss.filter(s => s.id !== selectedSold.id))
                    toastify("success", "Успешный возврат")
                    setModalShow(false)
                })
            } else {
                setLoading(false)
                setBtnDisabled(false)
                let ss = sold
                setSold(ss.filter(s => s.id !== selectedSold.id))
                toastify("success", "Успешное удаление")
                setModalShow(false)
            }
        }).catch(err => {
            setLoading(false)
            setBtnDisabled(false)
            console.log(err)
            toastify("error", "Ошибка")
        })
    }

    const updateSold = () => {
        setLoading(true)
        console.log("update", selectedSold.id)
        let payload = selectedSold
        delete payload.id
        setDoc(doc(fire, "sold" + branch, selectedSold.id), payload).then(res => {
            console.log("updated", res)
            setLoading(false)
            let ss = sold
            ss = ss.filter(s => s.id !== selectedSold.id)
            ss.unshift(selectedSold)
            setSold([...ss])
            toastify("success", "Успешное изменение")
            setModalShow(false)
        }).catch(err => {
            setLoading(false)
            console.log(err)
            toastify("error", "Ошибка")
        })
    }

    const editClient = () => {
        setNewClient(clientFilter)
        setClientModalShow(true)
    }

    const updateClient = () => {
        setLoading(true)

        const batch = writeBatch(fire)
        
        const clientPayload = {name: newClient?.name, phone: newClient.phone}
        batch.update(doc(fire, "clients" + branch, newClient?.id), clientPayload)
        
        sold.filter(s => s.client?.id === newClient.id).forEach(s => {
            let payload = {...s, client: {id: newClient?.id, name: newClient?.name, phone: newClient?.phone}}
            batch.update(doc(fire, "sold" + branch, s?.id), payload)
        })

        batch.commit().then(res => {
            toastify("success", "Успешная операция")
            setLoading(false)
            console.log(res)
            setClientModalShow(false)
        }).catch(err => {
            toastify("error", "Ошибка")
            setLoading(false)
            console.log(err)
        })
    }

    return (
        <CCard className="my-2 mx-auto">
            <CCardHeader>
                <CRow>
                    <CCol>
                        <CCardTitle>
                            {t("Продажи")}
                        </CCardTitle>
                    </CCol>
                    <CCol className="d-flex flex-row-reverse">
                        {clientFilter && mode==="admin" && <CButton onClick={editClient}>Изменить клиента</CButton>}
                    </CCol>
                </CRow>
            </CCardHeader>
            <CCardBody>
                {loading ? <MiniSpinner/> : 
                    <div>
                        <CRow className="my-2">
                            <CCol sm="3">
                                <CFormLabel>{t("С")}</CFormLabel>
                                <CFormInput type="date" onChange={e => setDateFrom(e.target.value)} value={dateFrom}/>
                            </CCol>
                            <CCol sm="3">
                                <CFormLabel>{t("По")}</CFormLabel>
                                <CFormInput type="date" onChange={e => setDateTo(e.target.value)} value={dateTo}/>
                            </CCol>
                            <CCol sm="3">
                                <CFormLabel>{t("Товар")}</CFormLabel>
                                <Select
                                    isClearable
                                    options={items.map(it => {return {label: it.name, value: it.id, ...it}})}
                                    onChange={res => setItemFilter(res)}
                                    value={itemFilter}
                                />
                            </CCol>
                            <CCol sm="3">
                                <CFormLabel>{t("Цвет")}</CFormLabel>
                                <Select
                                    isClearable
                                    options={itemFilter?.colors?.map(it => {return {label: it, value: it}})}
                                    onChange={res => setColorFilter(res)}
                                    value={colorFilter}
                                />
                            </CCol>
                            <CCol sm="3">
                                <CFormLabel>{t("Клиент")}</CFormLabel>
                                <Select
                                    isClearable
                                    options={clients.map(cl => {return {label: cl.name + " " + cl.phone, value: cl.id, ...cl}})}
                                    onChange={res => setClientFilter(res)}
                                    value={clientFilter}
                                />
                            </CCol>
                        </CRow>
                        <div className="table-responsive">
                        <CTable className="table">
                            <CTableHead>
                                <tr>
                                    <th></th>
                                    <th>{t("Дата")}</th>
                                    <th>{t("Клиент")}</th>
                                    <th>{t("Товар")}</th>
                                    <th>{t("Цвет")}</th>
                                    <th style={{width: "12%"}}>{t("Длина")}</th>
                                    <th>{t("Цена за м")}</th>
                                    <th>{t("Итоговая цена")}</th>
                                    {mode === "admin" && <th>{t("Действия")}</th>}
                                </tr>
                            </CTableHead>
                            <CTableBody>
                                {sold.filter(s => dateFrom ? Date.parse(s.soldAt.toDate()) >= Date.parse(dateAbsolute(dateFrom)) : true)
                                    .filter(s => dateTo ? Date.parse(s.soldAt.toDate()) <= Date.parse(dateAbsolute(dateTo, true)) : true)
                                    .filter(s => itemFilter ? s.item?.id === itemFilter?.id : true)
                                    .filter(s => colorFilter ? s.color === colorFilter?.label : true)
                                    .filter(s => clientFilter ? s.client?.id === clientFilter?.id : true)
                                    // .sort((a, b) => Date.parse(a.soldAt.toDate()) > Date.parse(b.soldAt.toDate()))
                                    .map((s, i) => 
                                        <tr key={i}  style={{verticalAlign: "bottom"}}>
                                            <td>{i+1}</td>
                                            <td>{dateFormatter(s.soldAt.toDate())}</td>
                                            <td>{s.client?.name}</td>
                                            <td>{s.item?.name}</td>
                                            <td>{s.color}</td>
                                            <td>{s.roll?.len} м</td>
                                            <td>{(parseFloat(s.pricePerM) || 0).toFixed(2)}</td>
                                            <td>{(parseFloat(s.pricePerM || 0) * parseFloat(s.roll?.len)).toFixed(2)}</td>
                                            {mode === "admin" && <td>
                                                <CButton color="warning" size='sm' className='m-1'
                                                    onClick={() => {
                                                        setSelectedSold(s)
                                                        setModalFor("update")
                                                        setModalShow(true)
                                                    }}
                                                >Редактировать</CButton>
                                                <CButton color="info" size='sm' className='m-1'
                                                    onClick={() => {
                                                        setSelectedSold(s)
                                                        setModalFor("return")
                                                        setModalShow(true)
                                                    }}
                                                >Возврат</CButton>
                                                <CButton color="danger" size='sm' className='m-1'
                                                    onClick={() => {
                                                        setSelectedSold(s)
                                                        setModalFor("delete")
                                                        setModalShow(true)
                                                    }}
                                                >Удалить</CButton>
                                            </td>}
                                        </tr>
                                    )}
                                    <tr>
                                        <td colSpan="5"><b>Итого</b></td>
                                        <td>
                                            <b>
                                            {
                                                sold.filter(s => dateFrom ? Date.parse(s.soldAt.toDate()) >= Date.parse(dateAbsolute(dateFrom)) : true)
                                                .filter(s => dateTo ? Date.parse(s.soldAt.toDate()) <= Date.parse(dateAbsolute(dateTo, true)) : true)
                                                .filter(s => itemFilter ? s.item?.id === itemFilter?.id : true)
                                                .filter(s => clientFilter ? s.client?.id === clientFilter?.id : true)
                                                .reduce((sum, s) => sum + parseFloat(s.roll?.len), 0).toFixed(2)
                                            } м
                                            </b>
                                        </td>
                                        <td></td>
                                        <td>
                                            <b>
                                            {
                                                sold.filter(s => dateFrom ? Date.parse(s.soldAt.toDate()) >= Date.parse(dateAbsolute(dateFrom)) : true)
                                                .filter(s => dateTo ? Date.parse(s.soldAt.toDate()) <= Date.parse(dateAbsolute(dateTo, true)) : true)
                                                .filter(s => itemFilter ? s.item?.id === itemFilter?.id : true)
                                                .filter(s => clientFilter ? s.client?.id === clientFilter?.id : true)
                                                .reduce((sum, s) => sum + (parseFloat(s.pricePerM || 0) * parseFloat(s.roll?.len)), 0).toFixed(2)
                                            }
                                            </b>
                                        </td>
                                    </tr>
                            </CTableBody>
                        </CTable>
                        </div>
                    </div>}
            </CCardBody>
            <CCardFooter>

            </CCardFooter>
            {selectedSold && <CModal visible={modalShow} onClose={() => setModalShow(false)}>
                <CModalBody>
                    {loading ? <MiniSpinner/> : (modalFor === "delete" || modalFor === "return" ) ?
                        <>
                            <p>Подвердите {modalFor === "delete" ? "удаление" : "возврат"}? ({selectedSold?.item?.name + " " + selectedSold?.color + " - " +selectedSold?.roll?.len + "м"})</p>
                            <div className='d-flex flex-row-reverse'>
                                <CButton color='success' onClick={() => deleteSold()} disabled={btnDisabled}>Подтвердить</CButton>
                            </div>
                        </> :
                        <>
                            <CFormLabel>Новая цена ({selectedSold?.item?.name + " " + selectedSold?.color + " - " +selectedSold?.roll?.len + "м"})</CFormLabel>
                            <CFormInput type="number" value={selectedSold?.pricePerM} className='my-2'
                                onChange={e => setSelectedSold({...selectedSold, pricePerM: e.target.value})}/>
                            <div className='d-flex flex-row-reverse'>
                                <CButton color='success' onClick={() => updateSold()}>Сохранить</CButton>
                            </div>
                        </>
                    }
                </CModalBody>
            </CModal>}
            {newClient && <CModal visible={clientModalShow} onClose={() => setClientModalShow(false)}>
                <CModalBody>
                    {loading ? <MiniSpinner/> : <>
                        <CFormLabel>Имя</CFormLabel>
                        <CFormInput type="text" value={newClient?.name} className='my-2'
                            onChange={e => setNewClient({...newClient, name: e.target.value})}/>
                        <CFormLabel>Телефон</CFormLabel>
                        <CFormInput type="text" value={newClient?.phone} className='my-2'
                            onChange={e => setNewClient({...newClient, phone: e.target.value})}/>
                        <div className='d-flex flex-row-reverse'>
                            <CButton className='mx-1' color='secondary' onClick={() => setClientModalShow(false)}>Закрыть</CButton>
                            <CButton className='mx-1' color='success' onClick={() => updateClient()}>Сохранить</CButton>
                        </div>
                    </>}
                </CModalBody>
            </CModal>}
        </CCard>
    )
}