import { CAlert, CButton, CCard, CCardBody, 
    CCardFooter, CCardHeader, CForm, 
    CFormInput, CFormLabel} from '@coreui/react'
import React, { useEffect, useState } from 'react'
import { collection, addDoc, doc, setDoc, getDoc } from "firebase/firestore";
import { fire, fireStorage } from "src/configs/fire";
import { useHistory, useParams } from 'react-router';
import { toastify } from 'src/helpers/toast';
import { useTranslation } from 'react-i18next';
import { getDownloadURL, ref, uploadBytesResumable } from 'firebase/storage';
import CIcon from '@coreui/icons-react';
import { cilX } from '@coreui/icons';
import { useSelector } from 'react-redux';
import {compressAccurately} from 'image-conversion';

export default function ItemForm() {

    const {t} = useTranslation()

    const params = useParams()
    const history = useHistory()

    const branch = useSelector(state => state.settings.branch)

    const [item, setItem] = useState({
        name: "",
        code: "",
        colors: [""]
    })

    const [loading, setLoading] = useState(false)
    const [response, setResponse] = useState(null)

    const [progress, setProgress] = useState(null)
    
    const [docRef, setDocRef] = useState(null)

    useEffect(() => {
        if(params.id) {
            const dRef = doc(fire, "items" + branch, params.id)
            setDocRef(dRef)
            getDoc(dRef).then(res => setItem(res.data()))
        }
    }, [params, branch])

    const handleChange = (e) => {
        let name = e.target.name
        let value = e.target.value
        setItem({...item, [name]: value})
    }

    const submitForm = () => {
        setLoading(true)
        let payload = item
        console.log(payload)
        const collectionRef = collection(fire, "items" + branch);
        (params.id ? setDoc(docRef, payload) : addDoc(collectionRef, payload)).then(res => {
            setResponse("success")
            setLoading(false)
            if(!params.id) {
                addDoc(collection(fire, "historia" + branch), {date: new Date(), item: payload, action: "создание"}).then(res => {
                    setItem({})
                    history.push("/items")
                })
            }
            toastify("success", "Operation successful")
            // add redirect 
        }).catch(err => {
            toastify("error", "Operation failed")
            setResponse("failed")
            setLoading(false)
            console.log(err)
        })
    }

    const [btnDisable, setBtnDisable] = useState(false)

    const uploadImage = (e) => {
        let image = e.target.files[0]
        console.log(new Date().getTime().toString())
        setBtnDisable(true)
        if(image?.size > 1000000) {
            toastify("info", "File size is too big")
            compressAccurately(image, 200).then(res=>{
                //The res in the promise is a compressed Blob type (which can be treated as a File type) file;
                console.log("compressed image", res);
                console.log(new File([res], image.name).size)
                image = new File([res], image.name)
                fetchImage(image)
              })
        } else {
            fetchImage(image)
        }
    }


    const fetchImage = (image) => {
        const storageRef = ref(fireStorage, `images/${new Date().getTime().toString()}`)
        const uploadTask = uploadBytesResumable(storageRef, image)
        uploadTask.on(
        "state_changed",
        snapshot => {
            const progress = Math.round(
            (snapshot.bytesTransferred / snapshot.totalBytes) * 100
            );
            setProgress(progress);
        },
        error => {
            console.log(error);
        },
        () => {
            getDownloadURL(uploadTask.snapshot.ref).then(url => {
                setItem({...item, "image": url})
                console.log(url)
                setBtnDisable(false)
            })
        })
    }

    return (
        <CCard className="my-2 w-75 mx-auto">
            <CCardHeader>
                {t("Товар")} - {t(params.id ? "редактирование" : "создание")}
            </CCardHeader>
            <CCardBody>
                
                {response && <CAlert color={response === "success" ? "success" : "danger"}>
                        {response === "success" ? "Успешно" : "Ошибка"}
                    </CAlert>}
        
                {loading ? 
                    <div>Загрузка...</div> : 
                    <CForm>
                        <CFormLabel>{t("Название")}</CFormLabel>
                        <CFormInput value={item.name || ""} onChange={handleChange} type="text" placeholder="Название" name="name"/>

                        <CFormLabel>{t("Цвет")}</CFormLabel>
                        {item.colors?.map((color, i) =>
                            <div className="d-flex" key={i}>
                                <CFormInput value={color || ""} onChange={(e) => {
                                    let cs = item.colors
                                    cs[i] = e.target.value
                                    setItem({...item, colors: cs})
                                }} 
                                type="text" placeholder="Цвет" name="color" className='my-1 mr-1'/> 
                                <CIcon icon={cilX} className="my-auto mx-1" style={{cursor: "pointer"}} 
                                    onClick={() => setItem({...item, colors: item.colors.filter(c => color !== c)})} />
                            </div>
                        )}
                        <CButton color="light" size="sm" className='my-2' 
                            onClick={() => setItem({...item, colors: [...item.colors, ""]})}>
                            {t("Добавить цвет")}
                        </CButton>
                        <div>
                        <CFormLabel>{t("Загрузить картину")}</CFormLabel>
                                <CFormInput onChange={e => uploadImage(e)} type="file" name="file"/>
                                {progress ? progress + " %" : ""}
                        </div>
                        <div className='mt-2'>
                            <img alt="" src={item.image} width="100"/>
                        </div>
                    </CForm>}
            </CCardBody>
            <CCardFooter className="d-flex flex-row-reverse">
                <CButton variant="outline" color="success" className="mx-1" 
                    disabled={loading || btnDisable} onClick={() => !loading && submitForm()}>{t(params.id ? "Сохранить" : "Создать")}</CButton>
                <CButton variant="outline" color="secondary" className="mx-1"
                    disabled={loading} onClick={() => history.push("/items")}>{t("Назад")}</CButton>
            </CCardFooter>
        </CCard>
    )
}