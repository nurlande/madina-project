import { CButton, CCard, CCardBody, CCardHeader, CListGroup, CListGroupItem, CTable, CTableBody} from '@coreui/react'
import React, { useEffect, useState } from 'react'
import { doc, getDoc } from "firebase/firestore";
import { fire} from "src/configs/fire";
import { useParams } from 'react-router';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import CIcon from '@coreui/icons-react';
import { cilPlus, cilMinus } from '@coreui/icons';
import { useHistory } from 'react-router-dom';
import SellModal from './SellModal';

export default function RollSell() {

    const {t} = useTranslation()

    const params = useParams()
    const history = useHistory()

    const rolls = useSelector(state => state.rolls.rolls)
    const branch = useSelector(state => state.settings.branch)

    const [item, setItem] = useState({
        name: "",
        colors: [],
        image: ""
    })

    const [color, setColor] = useState(null)
    const [selected, setSelected] = useState([])

    const [open, setOpen] = useState(false)
    const [loading, setLoading] = useState(false)

    useEffect(() => {
        if(params.id) {
            setLoading(true)
            const dRollRef = doc(fire, "items" + branch, params.id)
            getDoc(dRollRef).then(res => {
                setItem({...res.data(), id: params.id})
                setLoading(false)
            })
        }
        if(params.color) {
            setColor(params.color)
        }
    }, [params, branch])

    const updateSelected = (roll) => {
        if (selected.some(s => s.id === roll.id)) {
            setSelected(selected.filter(s => s.id !== roll.id))
        } else {
            setSelected([...selected, roll])
        }
    }

    const goBack = () => {
        history.push("/items/" + item?.id + "/sell")
    }

    return (
        <CCard className="my-2 w-75 mx-auto">
            <CCardHeader>
                {t("Рулоны")} Продажа: {color}
            </CCardHeader>
            <CCardBody>
                {loading ? 
                    <div>Загрузка...</div> : 
                    <div>
                        <div className='table-responsive-sm'>
                            <CTable className='table'>
                                <CTableBody>
                                    {rolls.filter(r => r.color === color && r.itemId === item?.id).map((r, i) =>
                                        <tr key={i}>
                                            <td style={{width: "95%"}}>
                                                {r.len} м
                                            </td>
                                            <td>
                                                <span onClick={() => updateSelected(r)} className='mx-auto' style={{cursor: "pointer"}}>
                                                    {selected.some(s => s.id === r.id) ? 
                                                    <CIcon icon={cilMinus}/> : <CIcon icon={cilPlus}/>}
                                                </span>
                                            </td>
                                        </tr>
                                    )}
                                </CTableBody>
                            </CTable>
                        </div>
                        <CListGroup className='d-none'>
                            {rolls.filter(r => r.color === color && r.itemId === item?.id).map((r, i) => 
                                <CListGroupItem key={i}>
                                    <div className='d-flex'>
                                        <span style={{width: "95%"}}>{r.len} м</span>
                                        <span onClick={() => updateSelected(r)} className='mx-auto'>
                                            {selected.some(s => s.id === r.id) ? 
                                            <CIcon icon={cilMinus}/> : <CIcon icon={cilPlus}/>}
                                        </span>
                                    </div>
                                </CListGroupItem>
                            )}
                        </CListGroup>
                        <div className='my-1'>
                            Всего: {selected.length + " шт " + selected.reduce((sum, s) => sum + parseFloat(s.len), 0)} м
                        </div>
                        <div className="my-2 d-flex flex-row-reverse">
                            <CButton className='mx-1' color="success" disabled={loading} onClick={() => setOpen(true)}>Продать</CButton>
                            <CButton className='mx-1' color="secondary" onClick={goBack}>Назад</CButton>
                        </div>
                    </div>}
            </CCardBody>
            <SellModal 
                selected={selected} setSelected={setSelected} 
                open={open} setOpen={setOpen} loading={loading} setLoading={setLoading}
                item={item} color={color}/>
        </CCard>
    )
}