
import "firebase/firestore"; // access firestore database service
import {getFirestore} from "firebase/firestore"; 
import {getStorage} from "firebase/storage"; 
import {getAuth} from "firebase/auth"; 
import { initializeApp } from 'firebase/app';

const firebaseConfig = {
  apiKey: "AIzaSyDq_7ewax2NnQu9FP-ED0TZLsWoU0wOzB8",
  authDomain: "madina01-28f02.firebaseapp.com",
  projectId: "madina01-28f02",
  storageBucket: "madina01-28f02.appspot.com",
  messagingSenderId: "391029231625",
  appId: "1:391029231625:web:456678fcc986fcaf1c68d8",
  measurementId: "G-SMZE9G3FES"
};

// Initialize Firebase

const app = initializeApp(firebaseConfig);

// firestore
export const fire = getFirestore(app)

// storage rules : request.auth != null
export const fireStorage = getStorage(app)

// auth
export const fireAuth = getAuth(app)