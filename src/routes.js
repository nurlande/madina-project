import HomePage from './pages/HomePage'
import ItemDetails from './pages/ItemDetail'
import ItemForm from './pages/ItemForm'
import Items from './pages/Items'
import ItemByColors from './pages/ItemByColors'
import RollStore from './pages/RollStore'
import Sold from './pages/Sold'
import RollSell from './pages/RollSell'
import RollShow from './pages/RollShow'
import SellForm from './pages/SellForm'
import Historia from './pages/Historia'

const routes = [
  { path: '/', exact: true, name: 'Home'},
  { path: '/home', name: 'Home', component: HomePage },

  { path: '/items', name: 'Items', component: Items, exact: true },
  { path: '/item-history', name: 'Items', component: Historia, exact: true },
  { path: '/items/create', name: 'Items Create', component: ItemForm, exact: true },
  { path: '/items/edit/:id', name: 'Items Create', component: ItemForm, exact: true },

  { path: '/items/:id', name: 'Item Details', component: ItemDetails, exact: true},
  { path: '/items/:id/:action/', name: 'Item Details', component: ItemByColors, exact: true},
  { path: '/items/:id/store/:color', name: 'Item Details', component: RollStore, exact: true},
  { path: '/items/:id/sell/:color', name: 'Item Details', component: RollSell, exact: true},
  { path: '/items/:id/show/:color', name: 'Item Details', component: RollShow, exact: true},

  { path: '/sell-form', name: 'Sell Form', component: SellForm, exact: true },
  { path: '/sold', name: 'Sold', component: Sold, exact: true }
]

export default routes
