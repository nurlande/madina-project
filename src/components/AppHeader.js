import React, { useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {
  CContainer,
  CHeader,
  CHeaderBrand,
  CHeaderNav,
  CHeaderToggler,
  CDropdown,
  CDropdownToggle,
  CDropdownMenu,
  CDropdownItem,
  CNavItem,
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { cilMenu } from '@coreui/icons'

import { AppHeaderDropdown } from './header/index'
import { setSidebarShow } from 'src/redux/actions/settingsActions'
import i18n from 'src/lang/lang'

const AppHeader = () => {

  const dispatch = useDispatch()
  
  const sidebarShow = useSelector((state) => state.settings.sidebarShow)
  const branch = useSelector((state) => state.settings.branch)

  const [lang, setLang] = useState(localStorage.getItem("lang"))

  const switchLang = (language) => {
    setLang(language);
    i18n.changeLanguage(language);
    localStorage.setItem('lang', language);
  }

  return (
    <CHeader position="sticky" className="mb-4">
      <CContainer>
        <CHeaderToggler 
          className="ps-1"
          onClick={() => dispatch(setSidebarShow(!sidebarShow))}
        >
          <CIcon icon={cilMenu} size="lg" />
        </CHeaderToggler>
        <CHeaderBrand className="mx-auto d-md-none" to="/">
          MADINA01
        </CHeaderBrand>
        <CHeaderNav className="d-none d-md-flex me-auto">
        </CHeaderNav>
        <CHeaderNav>
          <CNavItem>
            {branch === "almaty" ? "Алматы" : "Бишкек"}
          </CNavItem>
          <CDropdown variant="nav-item" alignment="end" className="my-auto d-none"> 
            <CDropdownToggle>
              {lang === "ru" ? 
              <>
                RU
              </>
            : <>
                KG
              </>
            }
            </CDropdownToggle>
            <CDropdownMenu placement="bottom-end">
                {
                  lang === "ky" ?
                  <CDropdownItem onClick={() => switchLang('ru')}>
                    Русский
                  </CDropdownItem>
                : <CDropdownItem onClick={() => switchLang('ky')}>
                    Кыргызча
                  </CDropdownItem> 
                }
            </CDropdownMenu>
          </CDropdown>
        </CHeaderNav>
        <CHeaderNav className="ms-3">
          <AppHeaderDropdown />
        </CHeaderNav>
      </CContainer>
      {/* <CHeaderDivider /> */}
      {/* <CContainer fluid>
        <AppBreadcrumb />
      </CContainer> */}
    </CHeader>
  )
}

export default AppHeader
