import React from 'react'
import {CSpinner} from "@coreui/react"

const MiniSpinner = (props) => <CSpinner color='warning' size={props.size}/>

export default MiniSpinner
