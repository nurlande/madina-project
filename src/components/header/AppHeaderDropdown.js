import React from 'react'
import {
  CAvatar,
  CDropdown,
  CDropdownDivider,
  CDropdownHeader,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
} from '@coreui/react'
import {
  cilLockLocked,
} from '@coreui/icons'
import CIcon from '@coreui/icons-react'

import { setMode, setSidebarShow } from 'src/redux/actions/settingsActions'
import { useDispatch, useSelector } from 'react-redux'
import { getAuth, signOut } from "firebase/auth";

const AppHeaderDropdown = () => {

  const dispatch = useDispatch()

  const mode = useSelector(state => state.settings.mode)

  const logOut = () => {
    const auth = getAuth();
    signOut(auth).then(() => {
      localStorage.removeItem("markaUser")
      dispatch(setSidebarShow(false))
      dispatch(setMode("client"))
    }).catch((error) => {
      console.log(error)
    });
    
  }

  return (
    <CDropdown variant="nav-item" alignment="end">
      <CDropdownToggle placement="bottom-end" className="py-0" caret={false}>
        <CAvatar src={"https://www.pngarts.com/files/6/User-Avatar-in-Suit-PNG.png"} size="md" />
      </CDropdownToggle>
      <CDropdownMenu className="pt-0" placement="bottom-end">
        <CDropdownHeader className="bg-light fw-semibold py-2">Профиль</CDropdownHeader>
        <CDropdownItem className="text-capitalize">
          {mode}
        </CDropdownItem>
        <CDropdownDivider />
        <CDropdownItem onClick={logOut} style={{cursor: "pointer"}}>
          <CIcon icon={cilLockLocked} className="me-2"/>
            Выйти
        </CDropdownItem>
      </CDropdownMenu>
    </CDropdown>
  )
}

export default AppHeaderDropdown
