import React from 'react'
import { CFooter } from '@coreui/react'

const AppFooter = () => {
  return (
    <CFooter>
      <div>
        {/* <a href="https://coreui.io" target="_blank" rel="noopener noreferrer">
          CoreUI
        </a> */}
        <span className="ms-1">&copy; 2021 NurlanDev</span>
      </div>
      <div className="ms-auto">
        <span className="me-1">Developed by</span>
        <a href="https://nurlan-dev.web.app" target="_blank" rel="noopener noreferrer">
          NurlanDev
        </a>
      </div>
    </CFooter>
  )
}

export default React.memo(AppFooter)
