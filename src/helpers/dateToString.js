export const dateFormatter = (date) => {
  let d = new Date(date);
  return (
    d.getDate() +
    "/" +
    (parseInt(d.getMonth()) + 1) +
    "/" +
    d.getFullYear() +
    "  " +
    (d.getHours() < 10 ? "0" : "") +
    d.getHours() +
    ":" +
    (d.getMinutes() < 10 ? "0" : "") +
    d.getMinutes()
  );
};

export const dateAbsolute = (date, to) => {
  let d = new Date(date);
  d.setHours(to ? 23 : 0);
  d.setMinutes(to ? 59 : 0);
  return d;
};
