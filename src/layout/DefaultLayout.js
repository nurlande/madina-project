import { getAuth, onAuthStateChanged } from 'firebase/auth'
import { collection, doc, getDoc, onSnapshot, query } from 'firebase/firestore'
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import FullSpinner from 'src/components/spinners/FullSpinner'
import { fire } from 'src/configs/fire'
import Login from 'src/pages/auth/Login'
import { setRolls } from 'src/redux/actions/rollsActions'
import { setBranch, setMode } from 'src/redux/actions/settingsActions'
import { AppContent, AppSidebar, AppFooter, AppHeader } from '../components/index'

const DefaultLayout = () => {
  const dispatch = useDispatch()

  const [loading, setLoading] = useState(false)
  const [authUser, setAuthUser] = useState(null)

  const branch = useSelector(state => state.settings.branch)

  useEffect(() => {
    setLoading(true)

    const auth = getAuth();
    onAuthStateChanged(auth, (user) => {
      if (user) {
        setAuthUser(user)
        const dRef = doc(fire, "users", user.uid)        
        getDoc(dRef).then(res => {
          dispatch(res.data().role === "admin" ? setMode("admin") : setMode("viewer"))
          dispatch(res.data().branch === "almaty" ? setBranch("almaty") : setBranch(""))
          setLoading(false)
        }).catch(err => {
          setLoading(false)
        })
      } else {
        setAuthUser(null)
        setLoading(false)
      }
    });
  }, [dispatch])

  useEffect(()=> {
      const q = query(collection(fire, "rolls" + branch))
      onSnapshot(q, (querySnapshot) => {
        dispatch(
          setRolls(
            querySnapshot.docs.map(d => {
              return {id: d.id, ...d.data()}
            })
          )
        )
      })
  }, [dispatch, branch])
  
  return (
    <div>
      {loading ? <FullSpinner/> :
        (authUser ? 
          <div>
            <AppSidebar />
            <div className="wrapper d-flex flex-column min-vh-100 bg-light">
              
              <AppHeader />
              <div className="body flex-grow-1 px-3">
                <AppContent />
              </div>
              <AppFooter />
            </div>
          </div> : <Login />)}
    </div>
  )
}

export default DefaultLayout
